package hu.braininghub.swing.calculator;

import java.applet.*;
import java.awt.*;
/*
	GridLayout-ra p�lda
*/
public class Ingredients2 extends Applet {
  TextField t;
  double price = 300;
  public void init() {
    this.setLayout(new GridLayout(11,1));
    this.add(new Label("Mit k�r a pizz�j�ra?", Label.CENTER));
    this.add(new Checkbox("Cs�p�s paprika"));
    this.add(new Checkbox("Olivanogy�"));
    this.add(new Checkbox("V�r�shagyma"));
    this.add(new Checkbox("Sz�sz"));
    this.add(new Checkbox("Fokhagyma"));
    this.add(new Checkbox("Extra sajt"));
    this.add(new Checkbox("Kolb�sz"));
    this.add(new Checkbox("Anan�sz"));
    this.add(new Checkbox("Gy�mb�r"));
    this.t = new TextField(String.valueOf(price) + " Ft");

    this.t.setEditable(false); 
    this.add(this.t);
  }
}