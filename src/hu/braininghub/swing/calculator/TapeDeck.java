package hu.braininghub.swing.calculator;

import java.applet.*;    
import java.awt.*;
/*
	Flowlayout-ra p�lda
*/
public class TapeDeck extends Applet {

  public void init() {   
    this.setLayout(new FlowLayout());
    this.add( new Button("Play"));
    this.add( new Button("Rewind"));
    this.add( new Button("Fast Forward"));
    this.add( new Button("Pause"));
    this.add( new Button("Stop"));
  }
  
}