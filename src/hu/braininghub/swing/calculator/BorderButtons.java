package hu.braininghub.swing.calculator;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.TextArea;

/*
  	Borderlayout-ra p�lda
 */

public class BorderButtons extends Applet {
  public void init() {
    this.setLayout(new BorderLayout(20, 10));
    this.add("North", new Button("North"));
    this.add("South", new Button("South"));
    this.add("Center", new TextArea("Center"));
    this.add("East", new Button("East"));
    this.add("West", new Button("West"));
  }
}